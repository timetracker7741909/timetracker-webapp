FROM node:16-alpine AS build
WORKDIR /home/node/timetracker
USER node
COPY --chown=node:node . .
RUN npm install && npm run build

FROM nginx
COPY --from=build /home/node/timetracker/build /usr/share/nginx/html
