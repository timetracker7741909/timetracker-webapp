import { TimerDto } from "./services/timerService";

export function formatDuration(s: number): string {
	const f = (n: number) => `${Math.floor(n)}`.padStart(2, '0');

	const h = s / (60 * 60);
	s %= (60 * 60);
	const m = s / 60;
	s %= 60;

	return `${f(h)}:${f(m)}:${f(s)}`;
}

export function getCurrentRun(t: TimerDto): number {
	if(t.startedTime) {
		if(t.stoppedTime)
			return t.stoppedTime - t.startedTime;

		return Date.now() / 1000 - t.startedTime;
	}

	return 0;
}

export function translateInPeriod(x: number, d: number, period: number) {
	return (period + x + d) % period;
}

export function okOrThrow(): (resp: Response) => Response {
	return resp => {
		if(!resp.ok)
			throw { status: resp.status, message: resp.statusText };

		return resp;
	};
}
