import { okOrThrow } from "../utils";

const TIMETRACKER_API_URL = 'http://localhost:8080/timers';

export type CreateTimerDto = {
	title: string,
};

export type UpdateTimerDto = {
	title?: string,
	startedTime?: number,
	stoppedTime?: number,
};

export type RecordDto = {
	id: number,
	startedTime: number,
	stoppedTime?: number,
};

export type TimerDto = {
	id: number,
	title: string,
	startedTime?: number,
	stoppedTime?: number,
	duration: number,
}

export function createTimer(timer: CreateTimerDto): Promise<TimerDto> {
	return jsonFetch(TIMETRACKER_API_URL, 'POST', timer);
}

export function updateTimer(id: number, timer: UpdateTimerDto): Promise<TimerDto> {
	return jsonFetch(`${TIMETRACKER_API_URL}/${id}`, 'PATCH', timer);
}

export function getTimer(id: number): Promise<TimerDto> {
	return jsonFetch(`${TIMETRACKER_API_URL}/${id}`);
}

export function getTimers(): Promise<Array<TimerDto>> {
	return jsonFetch(TIMETRACKER_API_URL);
}

export function getRecentRecordsFor(timerId: number): Promise<Array<RecordDto>> {
	return jsonFetch(`${TIMETRACKER_API_URL}/${timerId}/records/recent`);
}

export function deleteTimer(timerId: number): Promise<number> {
	return jsonFetch(`${TIMETRACKER_API_URL}/${timerId}`, 'DELETE');
}

function jsonFetch<T>(url: string, method: string = 'GET', body?: any): Promise<T> {
	let req: any = { method };
	if(body) {
		req = {
			...req,
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(body)
		}
	}

	return fetch(url, req)
		.then(okOrThrow())
		.then(r => r.json());
}
