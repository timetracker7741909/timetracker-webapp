import { ChangeEvent, FormEvent, useEffect, useState } from "react";
import { Button, Card, Form, ListGroup, Spinner, Stack } from "react-bootstrap";
import { deleteTimer, getRecentRecordsFor, RecordDto, TimerDto, updateTimer } from "../services/timerService";
import { formatDuration, getCurrentRun } from "../utils";

function RecordListItem(props: { record: RecordDto }) {
	const started = new Date(props.record.startedTime * 1000);
	let stopped: Date | null = null;
	let diff = '0';

	if(props.record.stoppedTime) {
		stopped = new Date(props.record.stoppedTime * 1000);
		diff = formatDuration(props.record.stoppedTime - props.record.startedTime);
	}

	const locale = window.navigator.language;

	return (
		<ListGroup.Item>
			{started.toLocaleString(locale)} - {stopped?.toLocaleString(locale)} <b>(+{diff})</b>
		</ListGroup.Item>
	);
}

function RecordList(props: { records: Array<RecordDto> | null }) {
	if(!props.records)
		return <Spinner animation="border" className="m-auto" />;

	if(props.records.length === 0)
		return <p className="m-0">No records yet</p>

	return (
		<ListGroup>
			{props.records.map(r => <RecordListItem key={r.id} record={r} />)}
		</ListGroup>
	);
}

export type TimerDetailsProps = {
	timer: TimerDto,
	onUpdate: (t: TimerDto) => void,
	onDelete: (tid: number) => void,
};

export default function TimerDetails(props: TimerDetailsProps) {

	const [title, setTitle] = useState('');
	const [running, setRunning] = useState(false);
	const [currentRun, setCurrentRun] = useState(0);
	const [records, setRecords] = useState<Array<RecordDto> | null>(null);
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		setTitle(props.timer.title);
		setRunning(props.timer.startedTime !== null && props.timer.stoppedTime === null);
		setCurrentRun(getCurrentRun(props.timer));

		getRecentRecordsFor(props.timer.id)
			.then(setRecords);

	}, [props.timer]);

	useEffect(() => {
		if(running) {
			const handle = setTimeout(
				() => setCurrentRun(getCurrentRun(props.timer)), 300);

			return () => clearTimeout(handle);
		}
	}, [currentRun, running]);

	const handleToggle = () => {
		const time = Math.floor(Date.now() / 1000);
		const update = running ? { stoppedTime: time } : { startedTime: time };
		setRunning(v => !v);

		setLoading(true);
		updateTimer(props.timer.id, update)
			.then(t => props.onUpdate(t))
			.finally(() => setLoading(false));
	};

	const titleChanged = (e: ChangeEvent<HTMLInputElement>) => {
		setTitle(e.target.value);
	};

	const onSubmit = (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		if(title.length === 0)
			return;

		setLoading(true);
		updateTimer(props.timer.id, { title })
			.then(t => props.onUpdate(t))
			.finally(() => setLoading(false));
	};

	const handleDeleteTimer = () => {

		setLoading(true);
		deleteTimer(props.timer.id)
			.then(tid => props.onDelete(tid))
			.finally(() => setLoading(false));
	};

	return (
		<Card bg="dark" className="text-white shadow w-100">
			<Card.Body>
				<Stack gap={2}>
					<Button
						variant={running ? 'danger' : 'success'}
						onClick={handleToggle}
						disabled={loading}
						className="toggle-timer-button my-3 mx-auto">

						<div className="content">
							{running
								? <i className="bi bi-pause-fill"></i>
								: <i className="bi bi-play-fill"></i>}
						</div>
					</Button>

					<div className="timer-current-run text-success">
						+{formatDuration(currentRun)}
					</div>
					<div className="timer-duration">
						{formatDuration(props.timer.duration)}
					</div>

					<Form onSubmit={onSubmit} className="my-2">
						<Form.Control
							type="text"
							value={title}
							onChange={titleChanged}
							disabled={loading} />
					</Form>

					<Button variant="danger" onClick={handleDeleteTimer}>Delete timer</Button>

					Recent runs:
					<RecordList records={records} />
				</Stack>
			</Card.Body>
		</Card>
	);
}
