import { ChangeEvent, FormEvent, useState } from "react";
import { Button, Form, Spinner, Stack } from "react-bootstrap";
import { createTimer, TimerDto } from "../services/timerService";

export default function CreateTimerForm(props: { onCreated?: (t: TimerDto) => void }) {

	const [title, setTitle] = useState('');
	const [loading, setLoading] = useState(false);

	const onTitleChange = (e: ChangeEvent<HTMLInputElement>) => {
		if(!loading)
			setTitle(e.target.value);
	};

	const sanitizeTitle = () => {
		const sanitized = title.trim();
		setTitle(sanitized);
		return sanitized;
	}

	const onSubmit = (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		setLoading(true);

		const sanitized = sanitizeTitle();

		createTimer({ title: sanitized })
			.then(t => props.onCreated?.call(null, t))
			.then(() => setTitle(''))
			.finally(() => setLoading(false));
	};

	return (
		<Form onSubmit={onSubmit} className="py-3">
			<Stack direction="horizontal" gap={3}>
				<Form.Control
					type="text"
					value={title}
					onChange={onTitleChange}
					onBlur={sanitizeTitle}
					disabled={loading}
					placeholder="Create new timer..." />

				{loading && <Spinner as="span" animation="border" role="status" size="sm" />}
				<Button variant="primary" type="submit" disabled={loading || title.length === 0}>
					Create
				</Button>
			</Stack>
		</Form>
	);
}
