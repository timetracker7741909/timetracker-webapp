import { useEffect, useState } from "react";
import { Card, Stack } from "react-bootstrap";
import { TimerDto } from "../services/timerService";
import { formatDuration, getCurrentRun } from "../utils";

function TimerItem(props: { timer: TimerDto, isSelected: boolean, onSelected: (t: TimerDto) => void }) {

	const [currentRun, setCurrentRun] = useState(0);

	useEffect(() => {
		if(props.timer.startedTime === null)
			return;

		const handle = setTimeout(() => setCurrentRun(getCurrentRun(props.timer)), 300);

		return () => clearTimeout(handle);
	}, [currentRun, props.timer]);

	const selectTimer = () => {
		props.onSelected(props.timer);
	};

	const cardClasses = `timer-list-item my-2 ${props.isSelected ? 'shadow' : 'shadow-sm'}`;
	const running = (props.timer.startedTime !== null && props.timer.stoppedTime === null);

	return (
		<Card className={cardClasses} onClick={selectTimer} body={true}>
			<Stack direction="horizontal" gap={2}>
				<div>{running
					? <i className="bi bi-play-fill"></i>
					: <i className="bi bi-pause-fill"></i>}</div>
				<div className="me-auto">{props.timer.title}</div>
				{running && <div className="text-success"><b>+{formatDuration(getCurrentRun(props.timer))}</b></div>}
				<div>{formatDuration(props.timer.duration)}</div>
			</Stack>
		</Card>
	);
}

export type TimerListProps = {
	timers: Array<TimerDto>,
	selected: TimerDto | null,
	onSelected: (t: TimerDto) => void,
};

export default function TimerList(props: TimerListProps) {

	if(props.timers.length === 0)
		return <p>No timers found</p>;

	return (
		<Stack>
			{props.timers.map((t, idx) => <TimerItem
				key={t.id}
				timer={t}
				isSelected={t.id === props.selected?.id}
				onSelected={props.onSelected} />)}
		</Stack>
	);
}
