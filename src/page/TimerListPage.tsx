import { useEffect, useState } from "react";
import { Col, Container, Offcanvas, Row, Spinner } from "react-bootstrap";
import CreateTimerForm from "../components/CreateTimerForm";
import TimerDetails from "../components/TimerDetails";
import TimerList from "../components/TimerList";
import { getTimers, TimerDto } from "../services/timerService";
import { translateInPeriod } from "../utils";

export default function TimerListPage(props: any) {

	const [timers, setTimers] = useState<Array<TimerDto> | null>(null);
	const [selected, setSelected] = useState<TimerDto | null>(null);

	const moveSelect = (d: number) => {
		console.log(d);

		if(!timers)
			return;

		if(!selected) {
			setSelected(timers[0]);
			return;
		}

		const idx = translateInPeriod(
			timers.findIndex(t => t.id === selected.id), d, timers.length);
		setSelected(timers[idx]);
	};

	const handler = (e: KeyboardEvent) => {
		switch(e.keyCode) {
			case 40:	// down
				moveSelect(1);
				break;

			case 38:	// up
				moveSelect(-1);
				break;

			case 32:	// space

				break;
		}
	};

	useEffect(() => {
		window.addEventListener('keydown', handler, false);
		return () => window.removeEventListener('keydown', handler, false);
	});

	useEffect(() => {
		getTimers().then(ls => setTimers(ls));
	}, []);

	const handleCreated = (t: TimerDto) => setTimers([t].concat(...(timers ?? [])));

	const handleUpdateTimer = (updated: TimerDto) => {

		setTimers([updated].concat(timers!.filter(t => t.id !== updated.id)));

		if(selected?.id === updated.id)
			setSelected(updated);
	};

	const handleDeleteTimer = (tid: number) => {
		if(selected?.id === tid)
			setSelected(null);

		setTimers(timers ? timers.filter(t => t.id !== tid) : null);
	};

	const unselectTimer = () => setSelected(null);

	return (
		<Container>
			<Row>
				<Col>
					<h2>Your Timers</h2>
				</Col>
			</Row>

			<Row>
				<Col>
					<CreateTimerForm onCreated={handleCreated} />
					{(timers &&
						<TimerList
							timers={timers}
							selected={selected}
							onSelected={setSelected} />
					) || (
						<div><Spinner animation="border" size="sm" /> Loading timers...</div>
					)}
				</Col>

				{selected && <Col md={6} lg={4} className="d-none d-md-block">
					<Offcanvas
						show={true}
						placement="end"
						onHide={unselectTimer}
						responsive="md">

						<Offcanvas.Header closeButton />
						<Offcanvas.Body>
							<TimerDetails
								timer={selected}
								onUpdate={handleUpdateTimer}
								onDelete={handleDeleteTimer} />
						</Offcanvas.Body>
					</Offcanvas>
				</Col>}
			</Row>
		</Container>
	);
}
