import { Col, Container, Row } from "react-bootstrap";
import { Outlet } from "react-router-dom";

export default function Layout() {
	return (
		<Container fluid>
			<Row className="bg-dark text-white p-3">
				<Col>
					<h1 className="m-0">TimeTracker</h1>
				</Col>
			</Row>

			<Row className="p-3">
				<Col>
					<Outlet />
				</Col>
			</Row>
		</Container>
	);
}
