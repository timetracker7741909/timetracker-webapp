import React from 'react';
import { useRouteError } from "react-router-dom";

export default function ErrorPage() {
	const err: any = useRouteError();
	console.log('ERROR:', err);

	return (
		<div>
			<h2>Error</h2>
			<p>Error has occured</p>
			<p>{ err.statusText || err.message }</p>
		</div>
	);
}
